package com.javadude.widgets

import android.app.Application
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import java.util.concurrent.Executors

@ExperimentalComposeUiApi
class SampleViewModel(application: Application) : AndroidViewModel(application) {
    private val executor = Executors.newSingleThreadExecutor()
    private val db = Room.databaseBuilder(application, PersonDatabase::class.java, "PEOPLE").build()

    private val _selectedName = MutableStateFlow<String?>(null)
    val selectedName: Flow<String?>
        get() = _selectedName

    val people = db.personDao.getAll()

    fun addPerson(name : String) {
        executor.execute {
            db.personDao.save(Person().apply { this.name = name })
            updateWidgetList()
        }
    }

    fun updateSelectedName(name: String?) {
        _selectedName.value = name
    }

    private fun updateWidgetList() {
        // update the widget
        // note that this should go into your Repository class if you have one
        val application = getApplication<Application>()
        val appWidgetManager = AppWidgetManager.getInstance(application)
        val thisAppWidget = ComponentName(application.packageName, PersonAppWidgetProvider::class.java.name)
        val appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget)
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.people_list)
    }
}