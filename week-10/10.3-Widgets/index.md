---
title: Widgets
template: main-full.html
---

## Introduction

Users can personalize their home screens by adding "widgets", small user interfaces that interact with installed applications.

In this module, we'll create a simple application that manages a list of names and displays those names on a homescreen widget.

!!! note

    Note that the videos for this module use the old Android Views approach for the activity's user interface. I have updated the example code to use
    Jetpack compose for this (and Flows rather than LiveData), but unfortunately, Compose cannot yet be used to create homescreen widgets.
    When creating a widget user interface, you'll still need to use xml-defined layouts as described in the video and communicate with them using the "remote views" API.

    The new support for Jetpack Compose widgets is called "Glance" and is currently in Alpha. If you're interested, take a look here: [https://developer.android.com/jetpack/androidx/releases/glance](https://developer.android.com/jetpack/androidx/releases/glance)

    As an interesting aside... Note how much simpler the list of names UI is to implement in Jetpack Compose than the old RecyclerView!

## Videos

Total video time for this module: 42:32

            
### Widgets: Lecture (Spring 2016) (10:32)

<iframe width="800" height="450" src="https://www.youtube.com/embed/DY2Ij0lhDsE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                

### Widgets: Example (Spring 2016) (32:00)

<iframe width="800" height="450" src="https://www.youtube.com/embed/YtMB1HufaFY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Example Source

See: [https://gitlab.com/android-development-2022-refresh/widgets](https://gitlab.com/android-development-2022-refresh/widgets)
