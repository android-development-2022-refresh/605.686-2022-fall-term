---
title: Activities and Application Structure Overview
template: main-full.html
---

## Introduction

Activities are the main "screens" of your application. You can have a single activity and swap out the user-interface elements (which will be our strategy for this course), or navigate between multiple activities.

In this module, we'll look at activities from a high level and take a quick peek at how the "old school" View setup works. We won't be using Views (other than Google Map) for this class, but you should be familiar with them.

## Objectives

   * Understand how the Android system manages the lifecycle of an Activity, and some of the callbacks you can implement to become aware of changes in Activity state

   * Shudder when you see the XML that you used to have to write for user interfaces.

   * Smile knowing that the newer ways of writing user interfaces are much simpler and more direct.

## Videos

!!! note

    You can change the quality and playback speed by clicking the :material-cog: icon when the video is playing. 

    If you would like to search the captions for this video, click _Watch on YouTube_, press "..." and choose _Show Transcript_.

### Lecture: Activities

<iframe width="800" height="450" src="https://www.youtube.com/embed/sMr7xA5GQYE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Example: Activities

!!! note
   
    This video shows how activities _used_ to be created, to contrast with how we'll be creating applications in this class. Jetpack Compose makes thing much simpler, but this example is important, as you'll likely see applications using the old view-based approach. The application structure remains similar, as we'll use some XML resources (primarily string localizations).

<iframe width="800" height="450" src="https://www.youtube.com/embed/fFewMJHoYF4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Example Source

See [https://gitlab.com/android-development-2022-refresh/android-studio](https://gitlab.com/android-development-2022-refresh/android-studio)
