---
title:  Welcome!
template: main-full.html
---

!!! note

    All of the content in this Introduction module is critical to your success in this class. I know it's not the most thrilling read, but please be sure to read it thoroughly.

## Course Content

All course content (https://androidbyexample.com) and the course sample repository (https://gitlab.com/android-development-2022-refresh/605.686-2022-fall-term) are free for use to anyone. Feel free to pass these links to friends and co-workers.

For those viewing the content who are not enrolled in 605.686, please ignore any references to the Canvas site, assignments or syllabus.

## Your Actions

Please do the following

   * Check that your email and phone numbers are correct in JHED and SIS. I'll use these to send announcements. If you saw this announcement in your email you should be ok.

   * Please post in the "Introductions" forum as described in the thread header. Note that there is only one thread; you cannot create separate threads for your introduction.

## Welcome!
Hi there! I'm Scott Stanchfield. Welcome to 605.686, Mobile Application Development for the Android Platform! 
Let me give you an overview of what we will be doing this semester and where you should begin.

We'll be exploring Android application development, covering all sorts of topics including creation of user interfaces, storing data, and communicating with other devices and servers. You can see the full list of topics on this course site. Be sure to pay close attention to the Course Outline. It'll tell you which modules are covered each week as well as start and due dates for the assignments.

Please be sure to read this entire note, the syllabus (on the Canvas site), and all sections in the Introduction module. There are lots of very important things in there that I will hold you accountable for. In particular, please pay attention to the academic integrity (I'm good at catching cheaters!) and "Holistic" grading (so you understand what your grades actually mean) sections.

The syllabus is available via the "Syllabus" link on the left menu in Canvas. Note that we've migrated to a common syllabus management tool to ensure all the common university-level information is consistent and up-to-date across all courses. There's an Export button when viewing the Syllabus if you'd like to download and print it.

The Introduction module goes into more detail on assignment expectations and grading.

## Content

!!! note
    This applies to online sections of the course. If you are taking the class face-to-face/virtual-live,
    you are responsible for material covered in class (which will be recorded and posted), and the https://androidbyexample.com site is provided only as additional information.

I'm posting the course content that would normally appear in Canvas on https://androidbyexample.com. This allows a more custom experience. I'm trying out a new format that's less video-intensive, and hopefully will make it easier to locate information that you need. (Canvas currently doesn't allow custom CSS or Javascript and cannot support the features I'm trying to implement.)

I'm not updating _all_ of the material to this new format this term. When studying the content, text or video, please let me know which format works best for you.


## Sample Code

!!! note
    This applies to online sections of the course. If you are taking the class face-to-face/virtual-live,
    you are responsible for material covered in class (which will be recorded and posted), and the course samples site is only provided as supplemental material.

All sample code is available at https://gitlab.com/android-development-2022-refresh/605.686-2022-fall-term.

Please install git and clone this repository. As I am currently updating the course, you'll need to pull changes when I announce that new content is available.


## If you haven't used git before...

   1. Download and install git from https://git-scm.com/
 
   2. Create a directory to hold your git repositories. (I use c:\users\scott\git on my windows machine, but you can
      put it anywhere you would like)

   3. Open a command prompt/terminal and change to that directory
 
   4. Run

      ```
      git clone https://gitlab.com/android-development-2022-refresh/605.686-2022-fall-term.
      ```

     (This will create a `605.686-2022-fall-term` directory inside your git directory)

    5. Whenever I tell you to update the repository
       1. Open a command prompt/terminal and change to that android-development directory
       2. Run

          ```
          git pull
          ```

## Kotlin Note
All assignments must be written in Kotlin. The text and videos in the course will talk through development environment setup and implementation using Kotlin and describe some of the concepts behind Kotlin along the way.

I also teach a course in Kotlin (605.603) but it is not required for this course. It will, however, go into much more detail on the Kotlin language and I recommend everyone take it. Because, ya know, Kotlin is cool. Very, very cool

## Online Video Tips

Some modules will link to video content.

All videos are hosted on YouTube, and I've enabled automatic closed captions/transcript. The transcript is useful for searching. To search videos:

   1. Click the "..." button under the video

   2. Choose "Open Transcript"

   3. Pause the video

   4. Press control-f (or whatever your browser's "find" command is)

   5. Type what you want to find

   6. Click on the transcript line you want to jump to and the video will jump there.

   7. Press play

Note that the automatic transcripts are not perfect, but they can help you find what you're looking for (Eventually I'll be hand-editing them to improve accuracy).

All videos were recorded and rendered at 1920x1080 (HD). That's the ideal size to watch them (if you're watching full screen. If you're watching non full-screen, with the transcript, you'll want to have your resolution set higher to reduce artifacts. Make sure the settings for the video playback are HD/1080p for the best image. I've increased the font size in Android Studio when recording, but you'll want to be sure things are as clear as possible.

Keep in mind that YouTube allows you to change the playback speed. Some students like to speed up or (more likely, as I talk quickly at times) slow down a bit.

## Office Hours

Office Hours will be held each Monday from 7pm-8pm Eastern. Please use the "Office Hours" link from the Canvas site to enter the Zoom office hours session. Note that if no one shows up by 7:20p, I'll close the session (unless you have let me know in advance that you intend to come and will be late)

## Questions

If you have questions during the course, please:

   1. Search using Google or StackOverflow for ideas. You can often find a response faster than I will see your question and respond. 

   2. Check the forum and see if the question has been asked

   3. If the question isn't private (such as a question about your grades), please post it in the discussion forums

   4. If it's a private matter, please email me at scott.stanchfield@jhu.edu

Please use the forums as much as possible for questions; many students often have the same questions and this makes it easy for everyone to learn.

As you progress through the course, if you have comments about the content (things you really like, dislike, or how things can be done better ), please post in the Course Feedback forum or keep a log of your thoughts to send to me after the course is finished. (For those who are not enrolled in the course, please email me at scott@javadude.com with any comments or siggesti)

If you have any questions or concerns while going through this course you may post them in the General Questions thread, located under Discussions on the left menu in Canvas or you may contact me directly at scott.stanchfield@jhu.edu. Additionally, you can refer to Help & Support on the left menu for a listing of all the student services and support available to you.

Here's to a great term!
-- Scott