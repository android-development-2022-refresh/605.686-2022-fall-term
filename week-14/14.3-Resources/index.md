---
title: Resources
template: main-full.html
---

## Introduction

You've reached the end of the course! Congratulations! In this module, I'll point you to a few other resources that may be helpful and you work with Android in the future.

## Videos

Total video time for this module: 03:25

            
### Further Resources: Lecture (Summer 2021) (03:25)

<iframe width="800" height="450" src="https://www.youtube.com/embed/TmagDwUR594" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                
