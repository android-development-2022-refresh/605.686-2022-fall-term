---
title: Publishing
template: main-full.html
---

## Introduction

What good is an application if you keep it all to yourself? You can use the Google Play Store to distribute your application to others!

In this module, we'll work on uploading a release of an application on the play store and talk about some of the necessary setup and application details you may want to consider when publishing an application.

Note that you do not need to use the Google Play Store for distribution. You can publish an app on a web site for download, send it via email or on a USB drive. However, users must grant permission on their device to be able to install from non-Google-Play-Store locations.

## Videos

Total video time for this module: 28:00

            
### Publishing: Lecture (Summer 2021) (28:00)

<iframe width="800" height="450" src="https://www.youtube.com/embed/ouP17DyQsOM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

