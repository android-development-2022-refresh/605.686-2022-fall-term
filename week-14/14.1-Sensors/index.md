---
title: Sensors
template: main-full.html
---

## Introduction

Most Android devices come with sensors for reporting movement, temperature, direction, location, etc. In this module, we'll talk about some sensor concepts and implement a simple moving puck on the screen using accelerometer input that translates tilt of the phone into movement of the puck.

## Videos

Total video time for this module: 23:03

            
### Sensors: Lecture and Examples (Summer 2021) (23:03)

<iframe width="800" height="450" src="https://www.youtube.com/embed/lHtOM9lsAmg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Example Source

See: [https://gitlab.com/android-development-2022-refresh/sensors](https://gitlab.com/android-development-2022-refresh/sensors)
