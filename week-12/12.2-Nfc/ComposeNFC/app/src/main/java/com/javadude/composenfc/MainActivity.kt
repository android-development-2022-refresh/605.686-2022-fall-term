package com.javadude.composenfc

import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NdefMessage
import android.nfc.NdefRecord
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import com.javadude.composenfc.ui.theme.ComposeNFCTheme
import java.util.Locale

@ExperimentalComposeUiApi
class MainActivity : ComponentActivity() {
    private lateinit var pendingIntent: PendingIntent

    private val readFilters = arrayOf(
        IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED).apply {
            addDataScheme("http")
            addDataAuthority("javadude.com", null)
        },
        IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED, "text/plain")
    )

    private val writeFilters = emptyArray<IntentFilter>()

    private val writeTechList =
        arrayOf(arrayOf(Ndef::class.java.name), arrayOf(NdefFormatable::class.java.name))

    private var messageToWrite: NdefMessage? = null

    private var status by mutableStateOf("")
    private var input by mutableStateOf("")

    private fun intentFlags() =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent.FLAG_MUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        } else {
            PendingIntent.FLAG_UPDATE_CURRENT
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pendingIntent =
            PendingIntent.getActivity(
                this, 0,
                Intent(this, javaClass).apply {
                    addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                }, intentFlags()
            )

        setContent {
            ComposeNFCTheme {
                Surface(color = MaterialTheme.colors.background) {
                    Column {
                        Display(text = status)
                        CommonTextField(
                            label = getString(R.string.text_to_write),
                            value = input,
                            onValueChange = { input = it },
                            onBack = { finish() }
                        )
                        SimpleButton(text = getString(R.string.write_text), onClick = ::onWriteText)
                        SimpleButton(text = getString(R.string.write_uri), onClick = ::onWriteUri)
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        NfcAdapter
            .getDefaultAdapter(this@MainActivity)
            .enableForegroundDispatch(this@MainActivity, pendingIntent, readFilters, null)
    }
    override fun onPause() {
        NfcAdapter
            .getDefaultAdapter(this@MainActivity)
            .disableForegroundDispatch(this@MainActivity)
        super.onPause()
    }

    private fun enableWrite() {
        NfcAdapter.getDefaultAdapter(this).enableForegroundDispatch(this, pendingIntent, writeFilters, writeTechList)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        processNFC(intent)
    }

    private fun processNFC(intent: Intent) {
        if (messageToWrite != null) {
            writeTag(intent)
        } else {
            readTag(intent)
        }
    }

    private fun writeTag(intent: Intent) {
        intent.getParcelableExtra<Tag>(NfcAdapter.EXTRA_TAG)?.let { tag ->
            try {
                val ndef = Ndef.get(tag)
                if (ndef == null) {
                    NdefFormatable.get(tag)?.let { ndefFormatable ->
                        ndefFormatable.use {
                            it.connect()
                            it.format(messageToWrite)
                            Toast.makeText(this, getString(R.string.formatted_and_written), Toast.LENGTH_LONG).show()
                        }
                    } ?: throw IllegalStateException(getString(R.string.cannot_format)) // report more nicely in a real app...
                } else {
                    ndef.use {
                        it.connect()
                        it.writeNdefMessage(messageToWrite)
                        Toast.makeText(this, getString(R.string.tag_written), Toast.LENGTH_LONG).show()
                    }
                }
            } finally {
                messageToWrite = null
            }
        }
    }

    private fun readTag(intent: Intent) {
        status = ""
        intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)?.let { messages ->
            messages.forEach { message ->
                message as NdefMessage
                status = message.records.mapNotNull { record ->
                    when (record.tnf) {
                        NdefRecord.TNF_WELL_KNOWN -> {
                            when {
                                record.type.contentEquals(NdefRecord.RTD_TEXT) -> "WELL-KNOWN TEXT: ${String(record.payload)}"
                                record.type.contentEquals(NdefRecord.RTD_URI) -> "WELL-KNOWN URI: ${String(record.payload)}"
                                else -> null
                            }
                        }
                        else -> null
                    }
                }.joinToString("\n")
            }
        }
    }

    private fun onWriteText() {
        val record = NdefRecord.createTextRecord(Locale.getDefault().language, input)
        messageToWrite = NdefMessage(arrayOf(record))
        status = getString(R.string.tap_to_write_uri)

        enableWrite()

        // NOTE - if min API < 21, you'll need to create the record explicitly, as createTextRecord did not
        //        exist until 21...
        //
        //        val language = Locale.getDefault().language.toByteArray(charset("UTF-8"))
        //        val textArray = input.text.toString().toByteArray()
        //        val payload = ByteArray(textArray.size + language.size + 1)
        //
        //        payload[0] = 0x02 // UTF-8
        //        System.arraycopy(language, 0, payload, 1, language.size)
        //        System.arraycopy(textArray, 0, payload, 1 + language.size, textArray.size)
        //
        //        val record = NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, ByteArray(0), payload)
        //        messageToWrite = NdefMessage(arrayOf(record))
        //        text.text = getString(R.string.tap_to_write_text)
        //
        //        enableWrite()
    }

    private fun onWriteUri() {
        val record = NdefRecord.createUri(input)
        messageToWrite = NdefMessage(arrayOf(record))
        status = getString(R.string.tap_to_write_uri)
        enableWrite()
    }
}

