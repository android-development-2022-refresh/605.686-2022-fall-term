---
title: Testing
template: main-full.html
---

## Introduction

Automated tests can greatly improve the quality of your projects. You can run quick unit tests to check out non platform code, such as view models and algorithms, and instrumented tests that run on an emulator to test platform interaction such as user interfaces and databases.

In this module we'll add some tests to a simplified movies application.

!!! note

    Due to out-of-town trips, I decided to edit a recording of a live lecture that I gave Summer 2021. There are a few rough spots near the end of the first part of the example (live coding works well for me most of the time, but when it goes south, it does so spectacularly), but they're worked out in the second part of the example.

## Videos

Total video time for this module: 2:19:06

            
### Testing: Lecture (Summer 2021) (20:31)

<iframe width="800" height="450" src="https://www.youtube.com/embed/6MdN6GSjcSc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Testing: Example, Part 1 (Summer 2021) (1:02:04)

<iframe width="800" height="450" src="https://www.youtube.com/embed/k4aAgzQ4IyE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Testing: Example, Part 2 (Summer 2021) (56:31)

<iframe width="800" height="450" src="https://www.youtube.com/embed/Pd5W-YQoDrA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Example Source

See: [https://gitlab.com/android-development-2022-refresh/testing](https://gitlab.com/android-development-2022-refresh/testing)
