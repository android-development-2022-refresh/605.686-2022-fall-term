package com.javadude.speech

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun MyText(text: String, modifier: Modifier = Modifier) =
    Text(
        text = text,
        style = MaterialTheme.typography.h5,
        modifier = modifier
            .fillMaxWidth()
            .padding(8.dp)
    )