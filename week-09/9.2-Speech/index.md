---
title: Speech
template: main-full.html
---

## Introduction

Android applications can speak to you, and allow you to speak with them!

In this module, we'll look at the Android text-to-speech and speech-to-text APIs, and build a simple text-adventure game that can be played without typing!

## Videos

Total video time for this module: 51:23
            
### Speech: Overview (Fall 2021) (15:35)

<iframe width="800" height="450" src="https://www.youtube.com/embed/uTMbNvtmwZY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Speech: Example (Fall 2021) (35:48)

<iframe width="800" height="450" src="https://www.youtube.com/embed/YWtooRFWmZ8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Example Source

See: [https://gitlab.com/android-development-2022-refresh/speech](https://gitlab.com/android-development-2022-refresh/speech)
