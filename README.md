## Using this repository

This is the content site for Johns Hopkins University Whiting School of Engineering 605.686 Mobile Development for the Android Platform. All content is open source and free for use. Please see _LICENSE code_ and _LICENSE content_ for license details.

Content hosted here is generated using mkdocs and hosted at https://androidbyexample.com.

