---
title: Dependency Injection
template: main-full.html
---

Let's talk about dependencies.

A "dependency" is anything external to something else. So far, we've 
worked with _library dependencies_. 
These are bundles of third party code that gradle downloads for us and
makes accessible to the code we're writing. We can use types and functions
from that other code so we don't have to write everything ourselves.

But let's look a little deeper. Types and functions can have dependencies as well.

Using that same definition of "anything external", think about how a
type might use something external to it.

```kotlin
interface Phone { ... }

class SuperHappyFunAndroidPhone: Phone { ... }

class Person {
    private val phone: Phone = SuperHappyFunAndroidPhone()
}
```

`Phone` is another type that's external to `Person`. In this example,
the `Person` is creating the `Phone` that it's using. `Phone` is a
dependency used by `Person`.

In many cases, this might be perfectly fine. But think about it: the
`Person` will only _ever_ have that specific type of `Phone`, the 
`SuperHappyFunAndroidPhone`.

## Getting a Different Phone

So maybe we change the `val` to a `var`. This allows an external
object or function to change which `Phone` the `Person` is using,
but the `Person` still _starts_ with the original `SuperHappyFunAndroidPhone`.
Maybe we don't want to ever see a `SuperHappyFunAndroidPhone` when 
running, or maybe, depending on the circumstances, we sometimes want
to run with a `SuperHappyFunAndroidPhone` and other times run with a
different phone.

## Service Locator

What if we explicitly asked someone else to create the `Phone` for us?

```kotlin
interface Phone { ... }

object PhoneFactory {
    fun createPhone(): Phone { return SuperHappyFunAndroidPhone() }
}

class Person {
    private val phone: Phone = PhoneFactory.createPhone()
}
```

That's much better from the `Person`'s point of view. The `Person` 
only cares that they're using a `Phone`; they don't care about the 
specific type of phone being used. Much more flexible.

This pattern is called a **"Service Locator"**. A class calls out to
an fixed external object (typically a **Singleton** instance of some 
class - that's what the `object` keyword in front of `PhoneFactory`
is doing). 

This does a few things for us:

* The `Person` never knows what kind of `Phone` they'll get;
  they only know or care about what they can do with an object that
  implements interface `Phone`
* The `Person` can _never_ be created without a `Phone` being
  created as well.
* The choice of the type of `Phone` is up to something else 
  (the `PhoneFactory`).

## Service Locator - Different Phones

However - we still have the issue of how to change which `Phone` is
being created. We could use a `var` lambda for `createPhone` in the `PhoneFactory`:

```kotlin
object PhoneFactory {
    var createPhone: () -> Phone = { SuperHappyFunAndroidPhone() }
}
```

This allows something else to _configure_ the `PhoneFactory` before
we create a `Person`.  This is really powerful, especially when you 
consider the most common alternative circumstances for use of a 
class like `Person`: Testing.

```kotlin
// application or test startup
PhoneFactory.createPhone = { SomeOtherPhone() }

val person = Person()
```

When testing `Person`, a good strategy is to try to use that `Person`
in as isolated a way as possible. We want to see if the logic in the
`Person` works; we're not interested in the logic of its dependencies.

By configuring our service locator to return some object that 
implements the `Phone` interface, we can isolate the `Person` for 
testing by passing in an alternative `Phone` implementation 
called a "test double". Typically this double would return fixed 
values when its functions are called and/or track calls to its 
functions so you could verify that the `Person` made the
expected calls.

!!! note

    We'll wave our hands here and move away from discussions of testing
    in this module. Think of testing as common alternative use of a
    class, possibly requiring different dependencies than normal use 
    of the class.

## Dependency Injection

But think about this for a moment - we've created an entire separate
object for the `Person` to call out and get a phone. What we're trying
to do is just pass a `Phone` to the `Person`. We can do that more
directly and cleanly by just passing the `Phone` to the constructor of
the `Person`:

```kotlin
interface Phone { ... }

class Person(val phone: Phone) { 
    ... 
}
```

This does a few things for us:

   * The `Person` still never knows what kind of `Phone` they'll get; 
     they only know or care about what they can do with an object that
     implements interface `Phone`
   * The `Person` still can _never_ be created without a `Phone` being
     passed to its constructor.
   * The choice of the type of `Phone` is up to whomever creates the
     `Person`. That choice can vary based on how the `Person` will be
     used.

Because _something else_ is passing in the needed dependencies, we
call this **"Dependency Injection"**.

Rather than the object that needs a dependency create or pull in
the dependency, something else pushes that dependency in. This makes
normal use and test use much more straightforward:

```kotlin
val person = Person(SuperHappyFunAndroidPhone())

val person = Person(SomeOtherPhone())

val person = Person(MockPhone())
```

## Types of Dependency Injection

### Constructor Injection

The first type of dependency injection that we've seen so far is called
**"Constructor Injection"**. Dependencies are declared in the 
constructor for an object and passed in. _It's impossible to create
an instance of the class without its dependencies_.

### Property/Setter Injection

But we can also pass dependencies as properties, known as 
**"Property Injection"** (alternatively **Setter Injection** or **Field Injection**)

```kotlin
class Person {
    var phone: Phone = ???
}

val person = Person().apply { 
    phone = SuperHappyFunAndroidPhone()
}
```

This is a scenario we demonstrated earlier. Think about:

   * What is a reasonable default for the `phone` (if there is one)?
   * Is it ok to create that default instance?
   * If the default instance is a dummy, placeholder, or Null Object,
     is it ok to use until the real desired value has been set?
     What if the value is never set (accidentally or intentionally)?

#### `Null` default
There are a few ways to deal with this. First, we could set the 
default value to `null`. This requires all uses to use `?.` or `?:` 
for safety, and isn't always desirable.

```kotlin
class Person {
    var phone: Phone? = null
}

val person = Person()
person?.phone?.call("867-5309")
```

!!! note

    In general, if you always expect something else to set a value
    before it's used, using a default of `null` is not a good idea.

#### Null Objects

We could employ the **Null Object** pattern here. **Null Object** is a
technique by which you define an actual object that does nothing or
always throws an exception. By using a real object instead of just
`null`, the code using that property is much simpler, as it no longer
needs null-safe operators like `?.` and `?:`. 

First, a typical **Null Object** approach:

```kotlin
object NullPhone1: Phone {
    // do nothing
    fun call(number: String) { } 
}

class Person {
    var phone: Phone = NullPhone1()
}

val person = Person()
person.phone.call("867-5309") // silently does nothing if phone not set
```

Alternatively, a **Null Object** that alerts you the value hasn't been set:

```kotlin
object NullPhone2: Phone {
    // throw exception
    fun call(number: String) {
        throw IllegalStateException("Phone not set!")
    } 
}

class Person {
    var phone: Phone = NullPhone2()
}

val person = Person()
person.phone.call("867-5309") // throws exception if phone not set
```

These are reasonable approaches, but Kotlin has another trick up its
sleeve: `lateinit` variables.

#### `lateinit` Variables

Often there are many properties that you know you'll always initialize 
before using them, but you don't have a reasonable default up front.

Making them nullable would mean that you have to constantly use 
null-safe access, which really isn't necessary. And any other default
value would never be used (so why define one?)

This is often the case with frameworks that have a well-defined 
lifecycle, such as Android. These frameworks define callbacks, such
as `onCreate()`, which give you an opportunity to initialize your
data before you use it.

Kotlin includes a `lateinit` keyword. It's a promise you make to the
compiler that you'll set it before you use it. Kotlin will trust you
at compile time, but verify at runtime that you've kept that promise.

This allows you to write

```kotlin
class Person {
    lateinit var phone: Phone
}
```

No initializer! You can then assign it and use it

```kotlin
val person = Person()

person.phone = SuperHappyFunAndroidPhone()
person.phone.call("867-5309")
```

Any code using the `phone` property doesn't need null-safe accessors.

!!! note

    If you don't set the value before using it, Kotlin will throw
    an `UninitializedPropertyAccessException`. You can use
    `person::phone.isInitialized` (or `this::phone.isInitialized` if
    inside the `Phone` class) if you need to check if it has
    been initialized yet, but you should avoid it unless absolutely
    necessary.

## So Many Options!

We've seen many ways you can approach dependencies; so what should we
choose?

It really boils down to three basic options

   * Create the dependency inside the class. Less flexible, but
     sometimes you don't need that flexibility.
   * Constructor Injection: Great option if the dependency _must_
     exist for the class instance to be valid.
   * Property Injection (use lateinit or a reasonable default):
     Great option if you cannot create the dependency instance at the
     same time as the class instance.

## Manual vs Automated

You don't need to do anything fancy to use Dependency Injection.
You can write code like

```kotlin
val person = Person(SuperHappyFunAndroidPhone())

// or

val person = Person()
person.phone = SuperHappyFunAndroidPhone()
```

and you get all of the flexibility of that dependency separation.

However, sometimes that's not convenient or even difficult to do. 
Sometimes the place where you need to create a `Person` doesn't 
_know_ what kind of `Phone` to create with it, or _might not have 
access_ to a means to create the `Phone`.

This is where automated Dependency Injection using a **Container**
becomes handy.

## Dependency-Injection Container

There are _many_ tools to assist Dependency Injection. Most take the 
form of a **Container**, an object to which you describe how to
create dependency instances, then ask to create objects for you.

You mark dependencies in your class definitions (usually using
**Annotations**), and the **Container** will create and inject those
dependencies for you.

For example, suppose we define our `Person` and `Phone` as follows:

```kotlin
interface Phone { ... }

class Person {
    @Inject lateinit var phone: Phone
}
```

The `@Inject` annotation tells the container that it needs to create
an instance of a `Phone` and set it after creating a `Person`.

We tell the container "when you need a `Phone`, create a 
`SuperHappyFunAndroidPhone`". This is called a **Binding**, and
completely separates the code that asks for a `Person` from the type
of `Phone` that is created and set for it.

Objects created by a container can form deeply-nested graphs,
referencing dependencies that themselves require dependencies.
By defining bindings, the class and interface definitions become
very loosely coupled and much more flexible for alternative uses
such as testing.

## On to an Example...

We'll be using **Hilt** as our Dependency-Injection container, but
we'll start by converting our Movie example to use manual dependency
injection.


Example Source: [https://gitlab.com/android-development-2022-refresh/dependency-injection](https://gitlab.com/android-development-2022-refresh/dependency-injection)






















