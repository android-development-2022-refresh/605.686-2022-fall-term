package com.javadude.movies

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import com.javadude.movies.screens.Ui
import com.javadude.movies.ui.theme.MoviesTheme
import dagger.hilt.android.AndroidEntryPoint

// ##START 030-entry
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
// ##END
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MoviesTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Ui {
                        finish() // handle "onExit"
                    }
                }
            }
        }
    }
}
