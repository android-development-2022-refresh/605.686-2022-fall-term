package com.javadude.movies.screens

import androidx.compose.runtime.Composable
import com.javadude.movies.components.SimpleText
import com.javadude.movies.repository.MovieDto

@Composable
fun MovieList(
    movies: List<MovieDto>
) {
    SimpleText(text = "Movies")
    movies.forEach {
        SimpleText(text = it.title)
    }
}
