package com.javadude.movies.screens

import androidx.compose.runtime.Composable
import com.javadude.movies.components.SimpleText
import com.javadude.movies.repository.RatingDto

@Composable
fun RatingList(
    ratings: List<RatingDto>,
    onRatingClick: (String) -> Unit,
) {
    SimpleText(text = "Ratings")
    ratings.forEach {
        SimpleText(text = it.name) {
            onRatingClick(it.id)
        }
    }
}
