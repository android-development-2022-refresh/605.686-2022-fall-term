---
title: Versions
template: main-full.html
---

## Introduction

Android is an incredibly fast-moving platform, and designing applications that can run successfully on multiple versions of Android can be tricky.

In this module, we'll talk about some concerns and techniques for creating backward-compatible applications.

## Objectives

* Know where to look to determine which versions of Android for which you want to retain compatibility
* See how version checks and compatibility libraries can assist with backward compatibility

## Video

!!! note

    You can change the quality and playback speed by clicking the :material-cog: icon when the video is playing. 

    If you would like to search the captions for this video, click _Watch on YouTube_, press "..." and choose _Show Transcript_.

<iframe width="800" height="450" src="https://www.youtube.com/embed/Y-f6X5iFxok" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

