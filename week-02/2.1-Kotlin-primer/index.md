---
title: Kotlin Primer
template: main-full.html
---

## Introduction

Kotlin is a fantastic new language that we'll be using to create our Android Applications. In this module, we'll quickly explore the basics of the language from a Java perspective.

During the rest of the course, I'll often explain some of these concepts in more detail.

## Objectives

* Understand the basic structure of Kotlin classes, properties and functions
* Use lambdas as arguments to functions as callbacks or observers

## Video

!!! note

    You can change the quality and playback speed by clicking the :material-cog: icon when the video is playing. 

    If you would like to search the captions for this video, click _Watch on YouTube_, press "..." and choose _Show Transcript_.

<iframe width="800" height="450" src="https://www.youtube.com/embed/wIhwlYdO7cQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Example Source

Example Source: [https://gitlab.com/android-development-2022-refresh/kotlin-primer](https://gitlab.com/android-development-2022-refresh/kotlin-primer)
