//package com.javadude.movies2
//
//import android.os.Bundle
//import android.widget.Toast
//import androidx.activity.ComponentActivity
//import androidx.activity.compose.BackHandler
//import androidx.activity.compose.setContent
//import androidx.activity.viewModels
//import androidx.compose.material.*
//import androidx.compose.material.icons.Icons
//import androidx.compose.material.icons.filled.*
//import androidx.compose.runtime.*
//import androidx.compose.ui.platform.LocalConfiguration
//import androidx.compose.ui.platform.LocalContext
//import androidx.compose.ui.res.stringResource
//import com.javadude.movies2.data.Actor
//import com.javadude.movies2.data.Role
//import com.javadude.movies2.ui.theme.Movies2Theme
//import kotlinx.coroutines.Dispatchers
//import kotlinx.coroutines.Job
//import kotlinx.coroutines.delay
//import kotlinx.coroutines.launch
//
//class MainActivity : ComponentActivity() {
//    private val viewModel by viewModels<MovieViewModel>()
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContent {
//            Movies2Theme {
//                Surface(color = MaterialTheme.colors.background) {
//                    // determine if we're at least 700dp wide - we'll use this to decide if
//                    //    we want to display side-by-side UIs
//                    val isWide = LocalConfiguration.current.screenWidthDp >= 700
//
//                    // at this level, we're just passing in the view model and necessary
//                    //    activity function (finish - used to quit when the stack is empty)
//                    Ui(
//                        isWide = isWide,
//                        viewModel = viewModel,
//                        onExit = { finish() }
//                    )
//                }
//            }
//        }
//    }
//}
//
///**
// * The main User Interface.
// *
// * At this level, we handle the decision of which UI to display based on the current screen state,
// *   and set up adapter functions to access the view model. We do this so the called functions
// *   won't need knowledge of the view model, which makes them easier to test but just passing in
// *   the data and functions they need, which can be easily mocked, rather than an entire view model.
// *
// * We also manage all navigation at this level. Ideally, each screen, is rendered with just a
// *   simple call to a @Composable function, passing functions that are called when the user
// *   performs actions that might trigger navigation. We pass lambdas that perform screen stack
// *   manipulations based on those user actions.
// *
// * No actual user interface creation (such as layouts, Text, Row...) should be called in this function.
// */
//@Composable
//fun Ui(
//    isWide: Boolean,
//    viewModel: MovieViewModel,
//    onExit: () -> Unit
//) {
//    val context = LocalContext.current
//    var currentToast by remember { mutableStateOf<Toast?>(null) }
//    val movieIconContentDescription = stringResource(id = R.string.movie_icon)
//    val actorIconContentDescription = stringResource(id = R.string.actor_icon)
//
//    val movies by viewModel.movies.collectAsState(initial = emptyList())
//    val actors by viewModel.actors.collectAsState(initial = emptyList())
//    val currentScreen by viewModel.currentScreen.collectAsState(initial = MovieListScreen(NoListSelection))
//    val scope = rememberCoroutineScope()
//
//    val undoManager = remember { UndoManager() }
//
//    val scaffoldState = rememberScaffoldState()
//
//    // The Ui level is the only one to touch the view model directly
//    // It pulls data from the view model and manages navigation (push/pop stack)
//    // All lower levels only act on data/functions passed to them
//    BackHandler {
//        // special case - if isWide and coming back to
//        //    MovieList from MovieDisplay or
//        //    ActorList from ActorDisplay
//        // skip the list
//        when {
//            isWide && currentScreen is MovieDisplayScreen -> viewModel.popSkipping<MovieListScreen>()
//            isWide && currentScreen is ActorDisplayScreen -> viewModel.popSkipping<ActorListScreen>()
//            else -> viewModel.pop()
//        }
//    }
//
//    // Set up adapter functions that convert calls to Fetchers properties to calls to the view model.
//    // This abstracts away the view model from the @Composable functions we call to create the UI
//    val fetchers = remember(viewModel) {
//        Fetchers(
////            getMovie = viewModel::getMovie, // function reference with explicit object instance
//            getMovie = { id -> viewModel.getMovie(id) },
//            getActor = { id -> viewModel.getActor(id) },
//            getCastFlow = { id -> viewModel.getCastFlow(id) },
//            getFilmographyFlow = { id -> viewModel.getFilmographyFlow(id) },
//        )
//    }
//
//    var showDeleteMoviesDialog by remember { mutableStateOf(false) }
//    var moviesToBeDeleted by remember {
//        mutableStateOf<List<String>>(emptyList())
//    }
//
//    if (showDeleteMoviesDialog) {
//        AlertDialog(
//            title = { Text(text = "Delete movies?")},
//            text = { Text(text = "Are you sure you want to delete the selected movies?")},
//            dismissButton = {
//                SimpleButton(text = "Cancel") {
//                    showDeleteMoviesDialog = false
//                }
//            },
//            confirmButton = {
//                SimpleButton(text = "Ok") {
//                    scope.launch {
//                        viewModel.deleteMoviesById(moviesToBeDeleted)
//                        viewModel.replaceTopSelection(NoListSelection)
//
//                        val text = context.resources.getQuantityString(
//                            R.plurals.movies_deleted,
//                            moviesToBeDeleted.size,
//                            moviesToBeDeleted.size
//                        )
//                        currentToast?.cancel()
//                        currentToast = Toast.makeText(context, text, Toast.LENGTH_LONG).apply {
//                            show()
//                        }
//                        moviesToBeDeleted = emptyList()
//                        showDeleteMoviesDialog = false
//                    }
//                }
//            },
//            onDismissRequest = { } // do nothing so accidental tap outside dialog doesn't dismiss
//        )
//    }
//
//    // Set up a common helper object to hold the set of properties and functions that are needed
//    //   to set up movie lists. This reduces the size of the parameter lists to our UI creation
//    //   functions (and the functions they call) and helps ensure consistency in how the list
//    //   data is used
//    val coreMovies = remember(isWide, movies, currentScreen) {
//        CoreInfo(
//            isWide = isWide,
//            items = movies,
//            getId = { it.id },
//            getText = { it.title },
//            icon = Icons.Filled.Movie,
//            iconDescription = movieIconContentDescription,
//            selection = currentScreen?.selection ?: NoListSelection,
//            multiSelectActions = {
//                IconButton(icon = Icons.Filled.Delete, iconDescription = stringResource(id = R.string.delete_movies)) {
//                    moviesToBeDeleted = it.ids
//                    showDeleteMoviesDialog = true
//
//// direct delete without going through dialog
////                    scope.launch {
////                        viewModel.deleteMoviesById(it.ids)
////                        viewModel.replaceTopSelection(NoListSelection)
////
////                        val text = context.resources.getQuantityString(R.plurals.movies_deleted, it.ids.size, it.ids.size)
////                        currentToast?.cancel()
////                        currentToast = Toast.makeText(context, text, Toast.LENGTH_LONG).apply {
////                            show()
////                        }
////                    }
//                }
//            },
//            singleSelectActions = {
//                IconButton(icon = Icons.Filled.Add, iconDescription = stringResource(id = R.string.add_movie)) {
//                    scope.launch {
//                        val movie = viewModel.createMovie()
//                        viewModel.push(MovieEditScreen(SingleListSelection(movie.id)))
//                    }
//                }
//                IconButton(icon = Icons.Filled.Person, iconDescription = stringResource(id = R.string.go_to_actor_list)) {
//                    scope.launch {
//                        viewModel.push(ActorListScreen(NoListSelection))
//                    }
//                }
//                IconButton(icon = Icons.Filled.Refresh, iconDescription = stringResource(id = R.string.reset_db)) {
//                    scope.launch {
//                        viewModel.resetDatabase()
//                    }
//                }
//            },
//        )
//    }
//
//    val undo = stringResource(R.string.undo)
//    // Set up a common helper object to hold the set of properties and functions that are needed
//    //   to set up actor lists. This reduces the size of the parameter lists to our UI creation
//    //   functions (and the functions they call) and helps ensure consistency in how the list
//    //   data is used
//    val coreActors = remember(isWide, actors, currentScreen) {
//        CoreInfo(
//            isWide = isWide,
//            items = actors,
//            getId = { it.id },
//            getText = { it.name },
//            icon = Icons.Filled.Person,
//            iconDescription = actorIconContentDescription,
//            selection = currentScreen?.selection ?: NoListSelection,
//            multiSelectActions = {
//                IconButton(icon = Icons.Filled.Delete, iconDescription = stringResource(R.string.delete_actors)) {
//                    scope.launch {
//                        undoManager.execute(DeleteActorsCommand(it.ids, viewModel))
//                        val text = context.resources.getQuantityString(R.plurals.actors_deleted, it.ids.size, it.ids.size)
//                        when (scaffoldState.snackbarHostState.showSnackbar(text, undo)) {
//                            SnackbarResult.Dismissed -> { } // do nothing
//                            SnackbarResult.ActionPerformed -> {
//                                undoManager.undo()
//                            }
//                        }
//                    }
////                    scope.launch {
////                        // example without undo manager
////                        DeleteActorsCommand(it.ids, viewModel).apply {
////                            execute()
////                            val text = context.resources.getQuantityString(R.plurals.actors_deleted, it.ids.size, it.ids.size)
////                            when (scaffoldState.snackbarHostState.showSnackbar(text, undo)) {
////                                SnackbarResult.Dismissed -> { } // do nothing
////                                SnackbarResult.ActionPerformed -> {
////                                    undo()
////                                }
////                            }
////                        }
////                    }
//                }
//            },
//            singleSelectActions = {
//                IconButton(icon = Icons.Filled.Add, iconDescription = stringResource(R.string.add_movie)) {
//                    scope.launch {
//                        val actor = viewModel.createActor()
//                        viewModel.push(ActorEditScreen(SingleListSelection(actor.id)))
//                    }
//                }
//                IconButton(icon = Icons.Filled.Movie, iconDescription = stringResource(R.string.go_to_movie_list)) {
//                    scope.launch {
//                        viewModel.push(MovieListScreen(NoListSelection))
//                    }
//                }
//                IconButton(icon = Icons.Filled.Undo, iconDescription = stringResource(R.string.undo)) {
//                    scope.launch {
//                        undoManager.undo()
//                    }
//                }
//                IconButton(icon = Icons.Filled.Redo, iconDescription = stringResource(R.string.redo)) {
//                    scope.launch {
//                        undoManager.redo()
//                    }
//                }
//            },
//        )
//    }
//
//    // Select which UI creation function we want to call based on the current screen state.
//    // The lambdas and properties passed at this level are all about accessing data from the
//    //    view model and calling view model functions. The core responsibility at this level is
//    //    navigation and general view model access
//    when (currentScreen) {
//        is MovieListScreen ->
//            MovieList(
//                coreInfo = coreMovies,
//                onSelectionChange = {
//                    when (it) {
//                        NoListSelection, is MultiListSelection -> {
//                            viewModel.replaceTopSelection(it)
//                        }
//                        is SingleListSelection -> {
//                            viewModel.replaceTopSelection(NoListSelection)
//                            viewModel.push(MovieDisplayScreen(it))
//                        }
//                    }
//                },
//            )
//        is MovieDisplayScreen -> {
//            MovieDisplay(
//                coreInfo = coreMovies,
//                onEditMovie = {
//                    viewModel.push(MovieEditScreen(SingleListSelection(it)))
//                },
//                fetchers = fetchers,
//                onSelectionChange = {
//                    viewModel.replaceTopSelection(it)
//                },
//                onSelectActor = {
//                    viewModel.push(ActorDisplayScreen(SingleListSelection(it)))
//                },
//                onDeleteRole = { actorId, movieId ->
//                    scope.launch {
//                        viewModel.deleteRole(actorId, movieId)
//                    }
//                },
//            )
//        }
//        is MovieEditScreen -> {
//            var updateJob: Job? = null
//            MovieEdit(
//                coreInfo = coreMovies,
//                onMovieChange = { movie ->
//                    // example of debouncing the update
//                    //   basically we wait until at least 500ms has passed between update
//                    //      requests before actually performing the database update
//                    //   each time we're asked to update, we wait 500ms before actually
//                    //     performing the update in the DB
//                    //   if another request comes in during that 500ms, we cancel the
//                    //     current request
//                    //   after 500ms passes, we blank out updateJob (so the current
//                    //     request will actually go to the database and cannot get canceled
//                    //     during the update)
//                    //   other requests to update after that start the clock again
//                    updateJob?.cancel()
//                    updateJob = scope.launch {
//                        delay(500)
//                        updateJob = null
//                        viewModel.update(movie)
//                    }
//                },
//                fetchers = fetchers,
//                onSelectionChange = {
//                    when (it) {
//                        NoListSelection, is MultiListSelection -> {
//                            viewModel.replaceTopSelection(it)
//                        }
//                        is SingleListSelection -> {
//                            viewModel.replaceTopSelection(NoListSelection)
//                            viewModel.push(MovieDisplayScreen(it))
//                        }
//                    }
//                },
//                onBack = { viewModel.pop() } // needed for TextField bug workaround
//            )
//        }
//        is ActorListScreen ->
//            ActorList(
//                scaffoldState = scaffoldState,
//                coreInfo = coreActors,
//                onSelectionChange = {
//                    when (it) {
//                        NoListSelection, is MultiListSelection -> {
//                            viewModel.replaceTopSelection(it)
//                        }
//                        is SingleListSelection -> {
//                            viewModel.replaceTopSelection(NoListSelection)
//                            viewModel.push(ActorDisplayScreen(it))
//                        }
//                    }
//                },
//            )
//
//        is ActorDisplayScreen ->
//            ActorDisplay(
//                coreInfo = coreActors,
//                onEditActor = {
//                    viewModel.push(ActorEditScreen(SingleListSelection(it)))
//                },
//                fetchers = fetchers,
//                onSelectionChange = {
//                    viewModel.replaceTopSelection(it)
//                },
//                onSelectMovie = {
//                    viewModel.push(MovieDisplayScreen(SingleListSelection(it)))
//                },
//                onDeleteRole = { actorId, movieId ->
//                    scope.launch {
//                        viewModel.deleteRole(actorId, movieId)
//                    }
//                },
//            )
//        is ActorEditScreen ->
//            ActorEdit(
//                coreInfo = coreActors,
//                onActorChange = { actor ->
//                    scope.launch {
//                        viewModel.update(actor)
//                    }
//                },
//                fetchers = fetchers,
//                onSelectionChange = {
//                    when (it) {
//                        NoListSelection, is MultiListSelection -> {
//                            viewModel.replaceTopSelection(it)
//                        }
//                        is SingleListSelection -> {
//                            viewModel.replaceTopSelection(NoListSelection)
//                            viewModel.push(ActorDisplayScreen(it))
//                        }
//                    }
//                },
//                onBack = { viewModel.pop() } // needed for TextField bug workaround
//            )
//
//        // when there is nothing left on the stack, exit the application
//        null -> onExit()
//    }
//}
//
//
//class DeleteActorsCommand(
//    val ids: List<String>,
//    private val viewModel: MovieViewModel
//): Command {
//    override val name: String
//        get() = "Delete Actors"
//
//    private lateinit var deletedActors: List<Actor>
//    private lateinit var deletedRoles: List<Role>
//
//    override suspend fun execute() {
//        deletedActors = viewModel.getActorsById(ids)
//        deletedRoles = viewModel.getRolesByActorId(ids)
//
//        viewModel.deleteActorsById(ids)
//        viewModel.replaceTopSelection(NoListSelection)
//    }
//
//    override suspend fun undo() {
//        viewModel.insert(*deletedActors.toTypedArray())
//        viewModel.insert(*deletedRoles.toTypedArray())
//    }
//}