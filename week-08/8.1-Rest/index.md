---
title: Rest
template: main-full.html
---

## Introduction

Android applications often communicate with servers to manage common data. In this module, we'll look at RESTful Web Services and see how your application can communicate with them to manage remote data.


## Videos

Total video time for this module: 25:48

            
### REST: Lecture (Fall 2021) (07:04)

<iframe width="800" height="450" src="https://www.youtube.com/embed/Fd0L5LbEwPU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### REST: Example (Fall 2021) (18:44)

<iframe width="800" height="450" src="https://www.youtube.com/embed/Ya7sCOzl0ss" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>            

## Example Source

See: [https://gitlab.com/android-development-2022-refresh/rest](https://gitlab.com/android-development-2022-refresh/rest)
