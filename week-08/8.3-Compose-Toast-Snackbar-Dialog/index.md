---
title: Compose Toast Snackbar Dialog
template: main-full.html
---

## Introduction

You'll often want to inform the user of the status of the application, or ask if the action they're taking is really what they want to do.

In this module, we'll look at three tehniques of giving feedback to the user: Toast, Snackbar and Dialog.

!!! note

    This video lecture is based on an earlier version of the Movies example, and the code has some different structure to it. (I'll eventually be replacing this module, but wanted to get a feel if the new text format was effective before creating the updated material.) Let me know if you have questions about the differences. The Toast/Snackbar/Dialog concepts will stay about the same in the newer version, and isn't used in an assignment.

## Videos

Total video time for this module: 1:59:27

            
### Jetpack Compose: Toast, Snackbar and Dialog Lecture (Fall 2021) (16:25)

<iframe width="800" height="450" src="https://www.youtube.com/embed/n5Mq-PwoXEM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Jetpack Compose: Toast, Snackbar and Dialog Example (Fall 2021) (1:43:02)

<iframe width="800" height="450" src="https://www.youtube.com/embed/xQeYd6fcqOk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                

## Example Source

See: [https://gitlab.com/android-development-2022-refresh/compose-toast-snackbar-dialog](https://gitlab.com/android-development-2022-refresh/compose-toast-snackbar-dialog)
